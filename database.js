// Our primary interface for the MongoDB instance
var MongoClient = require('mongodb').MongoClient;

// Used in order to verify correct returned values
var assert = require('assert');

/**
*
* @param databaseName - name of the database we are connecting to
* @param callback - callback to execute when the connection finishes
*
*/
connect = function(databaseName, callback) {
	// URL to the MongoDB we are connecting to
	var url = 'mongodb://localhost:27017/' + databaseName;


	// Connects to our MongoDB instance,
	// retrieve the selected database, and executes a callback on it
	MongoClient.connect(url, function(error, database) {
		// Make sure that no error was thrown
		assert.equal(null, error);

		console.log("Successfully connected to MongoDB instance !!\n\n");

		callback(database);
	});
};


/**
* Executes the find() method of the target collection in the
* target database, optionnally with a query.
* @param databaseName - name of the database
* @param collectionName - name of the collection
* @param query - optional query parameters for find()
*/
exports.find = function (databaseName, collectionName, query, callback) {
		connect(databaseName, function (database) {

		var collection = database.collection(collectionName);

		collection.find(query).toArray(
			function (err, documents) {
				// Make sure nothing went wrong
				assert.equal(err, null);

				callback(err, documents);
				// Print all the documents we found,
				//console.log("MongoDB returned the following documents: ");
				//console.dir(documents);
			}
		);

		database.close();
	});
};
