var db = require('./order_api/database.js');

module.exports = {
	database: "OrderBase",
	collection: "AccessTokens",

	generateToken = function(user, callback) {
		var token = {
			userId:user._id
		}

		// Persist and return the token
		db.insert(this.database, this.collection, token, function (err, res) {
			if (err) {
				callback(err, null);
			}
			else {
				callback(null, res);
			}
		});
	},

	authenticate = function(user, password, callback) {
		if (user.password == password) {
			// Create a new token for the user
			this.generateToken(user, function(err, res) {
				callback(null, res);
			});
		}

		else {
			callback({
				error: 'Athentication error',
				message: 'Incorrect Username or Password'
			}, null);
		}
	}
}











