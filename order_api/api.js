// Importing the database module
var database = require('../database');

// Import http module to enable comunications over the web
var http = require('http');

// Import URL module to customize paths used to query our API
var url = require('URL');



// Generic find methods (GET)
findAllResources = function (resourceName, req, res) {
		database.find('OrderBase', resourceName, {}, function (err, resources) {
		res.writeHead(200, {"Content-Type":"application/json"});
		res.end(JSON.stringify(resources));
	});
};


findResourceByName = function (resourceName, name, req, res) {
		database.find('OrderBase', resourceName, {'name':name}, function(err, resource) {
		res.writeHead(200, {"Content-Type":"application/json"});
		res.end(JSON.stringify(resource));
	});
};


// Products methods
findAllProducts = function(req, res) {
	findAllResources('Products', req, res);
};


findProductByName = function(name, req, res) {
	findResourceByName('Products', name, req, res);
};


// Generic insert/update methods (POST/PUT)
insertResource = function (resourceName, resource, req, res) {
	database.insert('OrderBase', resourceName, resource, function (err, resource) {
		res.writeHead(200, {"Content-Type":"application/json"});
		res.end(JSON.stringify(resource));
	});
};


// Product methods
insertProduct = function (product, req, res) {
	insertResource('Product', product, req, res, function (err, result) {
		res.writeHead(200, {"Content-Type":"application/json"});
		res.end(JSON.stringify(result));
	});
};



var server = http.createServer(function (req, res) {

	switch (req.url) {
		case '/api/products':
			if (req.method == 'GET') {
				var name = req.headers.name;

				if (name == null)
					findAllProducts(req, res);
				else {
					findProductByName(name, req, res)
				}
			}
			else {
				// Extract the data stored in the post body
				var body = '';
				req.on('data', function(dataChunk) {
					body += dataChunk;
				});

				req.on('end', function() {
					var postJSON = JSON.parse(body);
					//insertProduct(postJSON, req, res);
				});
			}

			break;
		default:
			res.end('\n\nError: Malformed API call\n\n');
	}
});



server.listen(8080);

console.log('\n\nUp, running, and ready for action !!\n\n');
